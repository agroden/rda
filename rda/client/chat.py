'''
Created on Jun 19, 2012

@author: alent
'''

from twisted.protocols import amp
from rda.common.commands import chat
from rda.common.network import RegisterProtocolPlugin

class ChatClientProtocol(amp.AMP):
    chatReceivedCallbacks = []
    addUserCallbacks = []
    removeUserCallbacks = []
    
    @chat.ChatMessage.responder
    def chatMessage(self, name, text, isPrivate):
        for cb in self.chatReceivedCallbacks:
            cb(name, text, isPrivate)
        return {}

    def addChatReceivedCallback(self, callback):
        self.chatReceivedCallbacks.append(callback)

    @chat.AddUser.responder
    def addUser(self, user):
        for cb in self.addUserCallbacks:
            cb(user)
        return {}

    def addAddUserCallback(self, callback):
        self.addUserCallbacks.append(callback)

    @chat.RemoveUser.responder
    def removeUser(self, user):
        for cb in self.removeUserCallbacks:
            cb(user)
        return {}

    def addRemoveUserCallback(self, callback):
        self.removeUserCallbacks.append(callback)


RegisterProtocolPlugin("Chat", ChatClientProtocol)