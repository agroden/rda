'''
Client protocol for handling random responses.

Created on Jun 24, 2012
@author: Alexander Groden
'''

from twisted.protocols.amp import AMP
from rda.common.commands import dice
from rda.common.network import RegisterProtocolPlugin

class DiceClientProtocol(AMP):
    """
    Client protocol for handling dice responses.
    """
    diceResultReceivedCallbacks = []
    
    @dice.DiceResult.responder
    def diceResult(self, name, values):
        for cb in self.diceResultReceivedCallbacks:
            cb(name, values)
        return {}
            
    def addDiceResultCallback(self, callback):
        self.diceResultReceivedCallbacks.append(callback)
        
RegisterProtocolPlugin("Dice", DiceClientProtocol)