'''
Created on Jun 19, 2012

@author: alent
'''

from twisted.internet import reactor
from twisted.internet.endpoints import TCP4ClientEndpoint
from rda.common.network import RDAProtocolFactory, NetworkInterface
from rda.common.commands.login import Login
from rda.common import utils
from rda.common import commands

def LogIn(protocol, uname, passwd):
    hashed = utils.hashPassword(passwd)
    d = protocol.callRemote(Login,
                     name=uname,
                     password=hashed,
                     version=commands.__version__)
    return d

class Client(NetworkInterface):
    """
    Client proxy for the L{NetworkInterface}.
    """
    
    def __init__(self):
        super(Client, self).__init__()
        
    def connectTo(self, host, port):
        """
        Connects to the server on the specified host and port.
        """
        self.endpoint = TCP4ClientEndpoint(reactor, host, port)
        d = self.endpoint.connect(RDAProtocolFactory())
        d.addCallback(self.gotProtocol)
        return d

    def gotProtocol(self, proto):
        self.protocol = proto