"""
.. module:: rda.gui.mainLogic
    :synopsis: 
.. moduleauthor:: Alexander Groden <alexander.groden@gmail.com>
"""

from datetime import datetime

from PySide.QtCore import QUrl, QCoreApplication, Slot
from PySide.QtDeclarative import QDeclarativeView

from rda.client.network import Client, LogIn
from rda.common.commands import chat, dice

class MainLogic(QDeclarativeView):
    def __init__(self, parent=None):
        super(MainLogic, self).__init__(parent)
        self.setWindowTitle("RDA")
        # Renders given file
        self.setSource(QUrl.fromLocalFile("gui/main.qml"))
        # QML resizes to main window
        self.setResizeMode(QDeclarativeView.SizeRootObjectToView)
        self.rootContext().setContextProperty("Logic", self)
        self.connected = False
        
    @Slot(str)
    def send(self, text):
        time = datetime.now()
        if not self.connected:
            self.rootObject().updateChat("[%s] System: Not currently connected" 
                                         % time.strftime("%H:%M"))
        else:    
            if text.startswith("/whisper "):
                sub = text[len("/whisper "):]
                name = sub[:sub.index(' ')]
                msg = sub[sub.index(' ') + 1:]
                d = Client().protocol.callRemote(chat.PrivateMessage, 
                                                 name = name, 
                                                 text = msg)
                d.addErrback(self.failedToChat)
            elif text.startswith("/roll "):
                sub = text[len("/roll "):]
                num = sub[:sub.index('d')]
                s = sub[(sub.index('d') + 1):]
                self.roll(num, s)
            else:
                d = Client().protocol.callRemote(chat.PublicMessage, 
                                                 text = text)
                d.addErrback(self.failedToChat)
        
        
    def doRoll(self, text):
        num = text[:text.index('d')]
        s = text[(text.index('d') + 1):]
        d = Client().protocol.callRemote(dice.DiceRequest, sides = s, number = num)
        d.addErrback(self.failedToChat)
    
    @Slot(int, int)
    def roll(self, dice, sides):
        print "rolling %s dice with %d sides" % (int(dice), int(sides))
        d = Client().protocol.callRemote(dice.DiceRequest, sides = sides, number = dice)
        d.addErrback(self.failedToChat)
    
    @Slot(str, str, str, str, str)
    def connectTo(self, name, host, port, uname, passwd):
        self.rootObject().updateChat("[System] Attempting to connect to %s at %s:%d as %s" % (name, host, int(port), uname))
        
    @Slot()
    def disconnect(self):
        print "disconnecting"
        
    @Slot()
    def close(self):
        QCoreApplication.instance().quit
        
    def closeEvent(self, event):
        self.disconnect()
        
    def failedToChat(self):
        print "Failed to chat"