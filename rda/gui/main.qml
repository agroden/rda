import QtQuick 1.1

Rectangle {
    id: window
    height: 768
    gradient: Gradient {
        GradientStop {
            position: 0
            color: "#9042aa"
        }

        GradientStop {
            position: 1
            color: "#08030a"
        }
    }
    width: 1024
    state: "DEFAULT"
    states: State {
        name: "DEFAULT"
        PropertyChanges { target: textInput; focus: true }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            window.state = "DEFAULT"
        }
    }

    // glare
    Rectangle {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 2
        radius: 4
        z: 1
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#64ffffff"
            }

            GradientStop {
                position: 1
                color: "#0008030a"
            }
        }
        smooth: true
        height: parent.height * 0.25
    }

    // dialogs
    Rectangle {
        id: connectDialog
        x: window.width / 2 - width / 2
        y: window.height / 2 - height / 2
        height: 312
        width: 192
        radius: 4
        border.color: "gold"
        border.width: 1
        color: "#7d000000"
        opacity: 0
        z: 1
        state: window.state

        states: [
            State {
                name: "CONNECT-DIALOG"
                PropertyChanges { target: connectDialog; opacity: 1 }
                PropertyChanges { target: name; focus: true }
            },
            State {
                name: "DEFAULT"
                PropertyChanges { target: connectDialog; opacity: 0 }
            }
        ]

        transitions: Transition {
            NumberAnimation {
                target: connectDialog
                properties: "opacity"
                easing.type: Easing.InOutQuad
                duration: 400
            }
        }

        MouseArea {
            anchors.fill: parent
            drag.target: parent
            drag.axis: Drag.XandYAxis
            drag.minimumX: 0
            drag.minimumY: menuBar.height
            drag.maximumX: window.width
            drag.maximumY: window.height
        }

        Text {
            id: connectTitle
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.margins: 4
            color: "white"
            text: "Connect"
        }

        Rectangle {
            id: connectTitleLine
            height: 1
            width: parent.width - 8
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: connectTitle.bottom
            anchors.margins: 4
            color: "gold"
        }

        Text {
            id: nameLabel
            anchors.top: connectTitleLine.bottom
            anchors.left: parent.left
            anchors.leftMargin: 4
            anchors.topMargin: 4
            color: "white"
            text: "Name"
        }

        Rectangle {
            id: nameBorder
            anchors.top: nameLabel.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width - 8
            height: 28
            radius: 4
            border.color: "gold"
            border.width: 1
            color: "transparent"

            TextInput {
                id: name
                color: "white"
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.left: parent.left
                anchors.margins: 4
                KeyNavigation.tab: host
            }
        }

        Text {
            id: hostLabel
            anchors.top: nameBorder.bottom
            anchors.left: parent.left
            anchors.leftMargin: 4
            anchors.topMargin: 4
            color: "white"
            text: "Server"
        }

        Rectangle {
            id: hostBorder
            anchors.top: hostLabel.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width - 8
            height: 28
            radius: 4
            border.color: "gold"
            border.width: 1
            color: "transparent"

            TextInput {
                id: host
                color: "white"
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.left: parent.left
                anchors.margins: 4
                KeyNavigation.tab: port
            }
        }

        Text {
            id: portLabel
            anchors.top: hostBorder.bottom
            anchors.left: parent.left
            anchors.leftMargin: 4
            anchors.topMargin: 4
            color: "white"
            text: "Port"
        }

        Rectangle {
            id: portBorder
            anchors.top: portLabel.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width - 8
            height: 28
            radius: 4
            border.color: "gold"
            border.width: 1
            color: "transparent"

            TextInput {
                id: port
                color: "white"
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.left: parent.left
                anchors.margins: 4
                KeyNavigation.tab: uname
                text: "9000"
            }
        }

        Text {
            id: unameLabel
            anchors.top: portBorder.bottom
            anchors.left: parent.left
            anchors.leftMargin: 4
            anchors.topMargin: 4
            color: "white"
            text: "User name"
        }

        Rectangle {
            id: unameBorder
            anchors.top: unameLabel.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width - 8
            height: 28
            radius: 4
            border.color: "gold"
            border.width: 1
            color: "transparent"

            TextInput {
                id: uname
                color: "white"
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.left: parent.left
                anchors.margins: 4
                KeyNavigation.tab: passwd
            }
        }

        Text {
            id: passwdLabel
            anchors.top: unameBorder.bottom
            anchors.left: parent.left
            anchors.leftMargin: 4
            anchors.topMargin: 4
            color: "white"
            text: "Password"
        }

        Rectangle {
            id: passwdBorder
            anchors.top: passwdLabel.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width - 8
            height: 28
            radius: 4
            border.color: "gold"
            border.width: 1
            color: "transparent"

            TextInput {
                id: passwd
                color: "white"
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.left: parent.left
                anchors.margins: 4
                echoMode: TextInput.Password
                passwordCharacter: "*"
                KeyNavigation.tab: connectOkButton
            }
        }

        Rectangle {
            id: connectOkButton
            border.color: "gold"
            border.width: 1
            radius: 4
            anchors.top: passwdBorder.bottom
            anchors.left: parent.left
            anchors.topMargin: 8
            anchors.leftMargin: 4
            color: "transparent"
            height: 28
            width: 88

            Text {
                color: "white"
                text: "OK"
                anchors.centerIn: parent
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    window.state = "DEFAULT"
                    Logic.connectTo(name.text, host.text, port.text, uname.text, passwd.text)
                }
            }
        }

        Rectangle {
            id: connectCancelButton
            border.color: "gold"
            border.width: 1
            radius: 4
            anchors.top: passwdBorder.bottom
            anchors.left: connectOkButton.right
            anchors.topMargin: 8
            anchors.leftMargin: 8
            color: "transparent"
            height: 28
            width: 88

            Text {
                color: "white"
                text: "Cancel"
                anchors.centerIn: parent
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    window.state = "DEFAULT"
                }
            }
        }
    }

    Rectangle {
        id: rollDialog
        x: window.width / 2 - width / 2
        y: window.height / 2 - height / 2
        height: 164
        width: 192
        radius: 4
        border.color: "gold"
        border.width: 1
        color: "#7d000000"
        opacity: 0
        z: 1
        state: window.state

        states: [
            State {
                name: "ROLL-DIALOG"
                PropertyChanges { target: rollDialog; opacity: 1 }
                PropertyChanges { target: dice; focus: true }
            },
            State {
                name: "DEFAULT"
                PropertyChanges { target: rollDialog; opacity: 0 }
            }
        ]

        transitions: Transition {
            NumberAnimation {
                target: rollDialog
                properties: "opacity"
                easing.type: Easing.InOutQuad
                duration: 400
            }
        }

        MouseArea {
            anchors.fill: parent
            drag.target: parent
            drag.axis: Drag.XandYAxis
            drag.minimumX: 0
            drag.minimumY: menuBar.height
            drag.maximumX: window.width
            drag.maximumY: window.height
        }

        Text {
            id: rollTitle
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.margins: 4
            color: "white"
            text: "Roll dice"
        }

        Rectangle {
            id: rollTitleLine
            height: 1
            width: parent.width - 8
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: rollTitle.bottom
            anchors.margins: 4
            color: "gold"
        }

        Text {
            id: diceLabel
            anchors.top: rollTitleLine.bottom
            anchors.left: parent.left
            anchors.leftMargin: 4
            anchors.topMargin: 4
            color: "white"
            text: "Number"
        }

        Rectangle {
            id: diceBorder
            anchors.top: diceLabel.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width - 8
            height: 28
            radius: 4
            border.color: "gold"
            border.width: 1
            color: "transparent"

            TextInput {
                id: dice
                color: "white"
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.left: parent.left
                anchors.margins: 4
                KeyNavigation.tab: sides
            }
        }

        Text {
            id: sidesLabel
            anchors.top: diceBorder.bottom
            anchors.left: parent.left
            anchors.leftMargin: 4
            anchors.topMargin: 4
            color: "white"
            text: "Sides"
        }

        Rectangle {
            id: sidesBorder
            anchors.top: sidesLabel.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width - 8
            height: 28
            radius: 4
            border.color: "gold"
            border.width: 1
            color: "transparent"

            TextInput {
                id: sides
                color: "white"
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.left: parent.left
                anchors.margins: 4
                KeyNavigation.tab: rollOkButton
            }
        }

        Rectangle {
            id: rollOkButton
            border.color: "gold"
            border.width: 1
            radius: 4
            anchors.top: sidesBorder.bottom
            anchors.left: parent.left
            anchors.topMargin: 8
            anchors.leftMargin: 4
            color: "transparent"
            height: 28
            width: 88

            Text {
                color: "white"
                text: "OK"
                anchors.centerIn: parent
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    window.state = "DEFAULT"
                    Logic.roll(dice.text, sides.text)
                }
            }
        }

        Rectangle {
            id: rollCancelButton
            border.color: "gold"
            border.width: 1
            radius: 4
            anchors.top: sidesBorder.bottom
            anchors.left: rollOkButton.right
            anchors.topMargin: 8
            anchors.leftMargin: 8
            color: "transparent"
            height: 28
            width: 88

            Text {
                color: "white"
                text: "Cancel"
                anchors.centerIn: parent
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    window.state = "DEFAULT"
                }
            }
        }
    }

    Rectangle {
        id: helpDialog
        x: window.width / 2 - width / 2
        y: window.height / 2 - height / 2
        height: 164
        width: 300
        radius: 4
        border.color: "gold"
        border.width: 1
        color: "#7d000000"
        opacity: 0
        z: 1
        state: window.state

        states: [
            State {
                name: "HELP-DIALOG"
                PropertyChanges { target: helpDialog; opacity: 1 }
            },
            State {
                name: "DEFAULT"
                PropertyChanges { target: helpDialog; opacity: 0 }
            }
        ]

        transitions: Transition {
            NumberAnimation {
                target: helpDialog
                properties: "opacity"
                easing.type: Easing.InOutQuad
                duration: 400
            }
        }

        MouseArea {
            anchors.fill: parent
            drag.target: parent
            drag.axis: Drag.XandYAxis
            drag.minimumX: 0
            drag.minimumY: menuBar.height
            drag.maximumX: window.width
            drag.maximumY: window.height
        }

        Text {
            id: helpTitle
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.margins: 4
            color: "white"
            text: "Help"
        }

        Rectangle {
            id: helpTitleLine
            height: 1
            width: parent.width - 8
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: helpTitle.bottom
            anchors.margins: 4
            color: "gold"
        }

        Text {
            id: commandsLabel
            anchors.top: helpTitleLine.bottom
            anchors.left: parent.left
            anchors.leftMargin: 4
            anchors.topMargin: 4
            color: "white"
            text: "Commands"
        }

        Text {
            id: whisperLabel
            anchors.top: commandsLabel.bottom
            anchors.left: parent.left
            anchors.leftMargin: 24
            anchors.topMargin: 4
            color: "white"
            text: "Whisper: /whisper [user] [msg]\nSends a private message to [user]"
        }

        Text {
            id: rollLabel
            anchors.top: whisperLabel.bottom
            anchors.left: parent.left
            anchors.leftMargin: 24
            anchors.topMargin: 4
            color: "white"
            text: "Roll dice: /roll [x]d[y]\nRolls [x] dice with [y] sides"
        }

        Rectangle {
            id: helpOkButton
            border.color: "gold"
            border.width: 1
            radius: 4
            anchors.top: rollLabel.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 8
            anchors.leftMargin: 4
            color: "transparent"
            height: 28
            width: 88

            Text {
                color: "white"
                text: "OK"
                anchors.centerIn: parent
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    window.state = "DEFAULT"
                }
            }
        }
    }

    Rectangle {
        id: aboutDialog
        x: window.width / 2 - width / 2
        y: window.height / 2 - height / 2
        height: 164
        width: 300
        radius: 4
        border.color: "gold"
        border.width: 1
        color: "#7d000000"
        opacity: 0
        z: 1
        state: window.state

        states: [
            State {
                name: "ABOUT-DIALOG"
                PropertyChanges { target: aboutDialog; opacity: 1 }
            },
            State {
                name: "DEFAULT"
                PropertyChanges { target: aboutDialog; opacity: 0 }
            }
        ]

        transitions: Transition {
            NumberAnimation {
                target: aboutDialog
                properties: "opacity"
                easing.type: Easing.InOutQuad
                duration: 400
            }
        }

        MouseArea {
            anchors.fill: parent
            drag.target: parent
            drag.axis: Drag.XandYAxis
            drag.minimumX: 0
            drag.minimumY: menuBar.height
            drag.maximumX: window.width
            drag.maximumY: window.height
        }

        Text {
            id: aboutTitle
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.margins: 4
            color: "white"
            text: "About"
        }

        Rectangle {
            id: aboutTitleLine
            height: 1
            width: parent.width - 8
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: aboutTitle.bottom
            anchors.margins: 4
            color: "gold"
        }

        Text {
            id: titleLabel
            anchors.top: aboutTitleLine.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.leftMargin: 4
            anchors.topMargin: 4
            color: "white"
            text: "The Resplendent Digital Assistant"
        }

        Text {
            id: authorLabel
            anchors.top: titleLabel.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.leftMargin: 4
            anchors.topMargin: 4
            color: "white"
            text: "Author: Alexander Groden"
        }

        Text {
            id: aboutLabel
            anchors.top: authorLabel.bottom
            anchors.left: parent.left
            anchors.leftMargin: 4
            anchors.topMargin: 12
            width: parent.width
            color: "white"
            wrapMode: Text.Wrap
            text: "Proof of concept for a networked Exalted tabletop game aid."
        }

        Rectangle {
            id: aboutOkButton
            border.color: "gold"
            border.width: 1
            radius: 4
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 8
            anchors.leftMargin: 4
            anchors.bottomMargin: 4
            color: "transparent"
            height: 28
            width: 88

            Text {
                color: "white"
                text: "OK"
                anchors.centerIn: parent
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    window.state = "DEFAULT"
                }
            }
        }
    }

    // Actual gui code
    Rectangle {
        id: menuBar
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: 32
        color: "transparent"
        z: 0.5
        state: window.state

        Rectangle {
            id: serverButton
            border.color: "gold"
            border.width: 1
            radius: 4
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.margins: 4
            color: "#c8000000"
            width: 100
            state: window.state

            Text {
                color: "white"
                text: "Server"
                anchors.centerIn: parent
            }

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    if (window.state != "SERVER-MENU")
                        window.state = "SERVER-MENU"
                    else
                        window.state = "DEFAULT"
                }
                onEntered: {
                    if (window.state != "DEFAULT" && window.state.indexOf("DIALOG") === -1)
                        window.state = "SERVER-MENU"
                }
            }

            states: [
                State {
                    name: "SERVER-MENU"
                    PropertyChanges { target: connectButton; opacity: 1 }
                    PropertyChanges { target: disconnectButton; opacity: 1 }
                    PropertyChanges { target: exitButton; opacity: 1 }
                },
                State {
                    name: "DEFAULT"
                    PropertyChanges { target: connectButton; opacity: 0 }
                    PropertyChanges { target: disconnectButton; opacity: 0 }
                    PropertyChanges { target: exitButton; opacity: 0 }
                }
            ]

            transitions: [
                Transition {
                    to: "SERVER-MENU"
                    NumberAnimation {
                        target: connectButton
                        properties: "opacity"
                        easing.type: Easing.InOutQuad
                        duration: 400
                    }
                    NumberAnimation {
                        target: disconnectButton
                        properties: "opacity"
                        easing.type: Easing.InOutQuad
                        duration: 600
                    }
                    NumberAnimation {
                        target: exitButton
                        properties: "opacity"
                        easing.type: Easing.InOutQuad
                        duration: 800
                    }
                },
                Transition {
                    from: "SERVER-MENU"
                    NumberAnimation {
                        target: connectButton
                        properties: "opacity"
                        easing.type: Easing.InOutQuad
                        duration: 800
                    }
                    NumberAnimation {
                        target: disconnectButton
                        properties: "opacity"
                        easing.type: Easing.InOutQuad
                        duration: 600
                    }
                    NumberAnimation {
                        target: exitButton
                        properties: "opacity"
                        easing.type: Easing.InOutQuad
                        duration: 400
                    }
                }
            ]

            Rectangle {
                id: connectButton
                border.color: "gold"
                border.width: 1
                radius: 4
                anchors.top: parent.bottom
                anchors.left: parent.left
                width: parent.width
                height: parent.height
                color: "#c8000000"
                anchors.topMargin: 4
                opacity: 0

                Text {
                    color: "white"
                    text: "Connect"
                    anchors.centerIn: parent
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        window.state = "CONNECT-DIALOG"
                    }
                }
            }

            Rectangle {
                id: disconnectButton
                border.color: "gold"
                border.width: 1
                radius: 4
                anchors.top: connectButton.bottom
                anchors.left: parent.left
                width: parent.width
                height: parent.height
                color: "#c8000000"
                anchors.topMargin: 4
                opacity: 0

                Text {
                    color: "white"
                    text: "Disconnect"
                    anchors.centerIn: parent
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        Logic.disconnect()
                        window.state = "DEFAULT"
                    }
                }
            }

            Rectangle {
                id: exitButton
                border.color: "gold"
                border.width: 1
                radius: 4
                anchors.top: disconnectButton.bottom
                anchors.left: parent.left
                width: parent.width
                height: parent.height
                color: "#c8000000"
                anchors.topMargin: 4
                opacity: 0

                Text {
                    color: "white"
                    text: "Exit"
                    anchors.centerIn: parent
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        window.state = "DEFAULT"
                        Logic.close()
                    }
                }
            }
        }

        Rectangle {
            id: helpMenu
            border.color: "gold"
            border.width: 1
            radius: 4
            anchors.left: serverButton.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.margins: 4
            anchors.rightMargin: 0
            color: "#c8000000"
            width: 100
            state: window.state

            Text {
                color: "white"
                text: "Help"
                anchors.centerIn: parent
            }

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    if (window.state != "HELP-MENU")
                        window.state = "HELP-MENU"
                    else
                        window.state = "DEFAULT"
                }
                onEntered: {
                    if (window.state != "DEFAULT" && window.state.indexOf("DIALOG") === -1)
                        window.state = "HELP-MENU"
                }
            }

            states: [
                State {
                    name: "HELP-MENU"
                    PropertyChanges { target: aboutMenuItem; opacity: 1 }
                    PropertyChanges { target: helpMenuItem; opacity: 1 }
                },
                State {
                    name: "DEFAULT"
                    PropertyChanges { target: aboutMenuItem; opacity: 0 }
                    PropertyChanges { target: helpMenuItem; opacity: 0 }
                }

            ]

            transitions: [
                Transition {
                    to: "HELP-MENU"
                    NumberAnimation {
                        target: aboutMenuItem
                        properties: "opacity"
                        easing.type: Easing.InOutQuad
                        duration: 400
                    }
                    NumberAnimation {
                        target: helpMenuItem
                        properties: "opacity"
                        easing.type: Easing.InOutQuad
                        duration: 600
                    }
                },
                Transition {
                    from: "HELP-MENU"
                    NumberAnimation {
                        target: aboutMenuItem
                        properties: "opacity"
                        easing.type: Easing.InOutQuad
                        duration: 600
                    }
                    NumberAnimation {
                        target: helpMenuItem
                        properties: "opacity"
                        easing.type: Easing.InOutQuad
                        duration: 400
                    }
                }
            ]

            Rectangle {
                id: aboutMenuItem
                border.color: "gold"
                border.width: 1
                radius: 4
                anchors.top: helpMenu.bottom
                anchors.left: parent.left
                width: parent.width
                height: parent.height
                color: "#c8000000"
                anchors.topMargin: 4
                opacity: 0

                Text {
                    color: "white"
                    text: "About"
                    anchors.centerIn: parent
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        window.state = "ABOUT-DIALOG"
                    }
                }
            }

            Rectangle {
                id: helpMenuItem
                border.color: "gold"
                border.width: 1
                radius: 4
                anchors.top: aboutMenuItem.bottom
                anchors.left: parent.left
                width: parent.width
                height: parent.height
                color: "#c8000000"
                anchors.topMargin: 4
                opacity: 0

                Text {
                    color: "white"
                    text: "Help"
                    anchors.centerIn: parent
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        window.state = "HELP-DIALOG"
                    }
                }
            }
        }
    }

    function updateChat(text) {
        chatText.text += text
    }

    Rectangle {
        id: chat
        anchors.top: menuBar.bottom
        anchors.bottom: input.top
        anchors.left: parent.left
        anchors.right: slider.left
        color: "transparent"

        Rectangle {
            id: chatBorder
            border.color: "gold"
            border.width: 1
            radius: 4
            anchors.fill: parent
            anchors.leftMargin: 4
            anchors.bottomMargin: 4
            color: "transparent"

            Flickable {
                id: chatFlickable
                anchors.fill: parent
                anchors.margins: 4
                contentWidth: chatText.width
                contentHeight: chatText.height
                flickableDirection: Flickable.VerticalFlick
                boundsBehavior: Flickable.StopAtBounds
                clip: true

                TextEdit {
                    id: chatText
                    width: chatBorder.width - 8
                    objectName: "chatText"
                    horizontalAlignment: Text.AlignLeft
                    wrapMode: TextEdit.Wrap
                    color: "white"
                    cursorVisible: false
                    readOnly: true
                    smooth: true
                    selectByMouse: true
                    mouseSelectionMode: TextEdit.SelectCharacters
                    persistentSelection: false
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        window.state = "DEFAULT"
                    }
                }
            }

            Rectangle {
                id: chatSlider
                color: "gold"
                width: 6
                radius: 2
                anchors.right: parent.right
                anchors.rightMargin: 1
                y: parent.y + (chatFlickable.visibleArea.yPosition * (chatFlickable.height)) + 2
                height: chatFlickable.visibleArea.heightRatio * (chatFlickable.height) + 4

                MouseArea {
                    anchors.fill: parent
                    drag.target: parent
                    drag.axis: Drag.YAxis
                    drag.minimumY: chatBorder.y +2
                    drag.maximumY: chatBorder.height - height - 1
                    onPositionChanged: {
                        if (pressedButtons == Qt.LeftButton) {
                            chatFlickable.contentY = chatSlider.y * chatFlickable.contentHeight / chatBorder.height
                            window.state = "DEFAULT"
                        }
                    }
                    onClicked: {
                        window.state = "DEFAULT"
                    }
                }
            }
        }
    }

    Rectangle {
        id: slider
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        x: window.width - (128 + width)
        width: 8
        color: "transparent"

        Rectangle {
            height: 12
            anchors.centerIn: parent
            anchors.horizontalCenterOffset: -2

            Column {
                anchors.left: parent.left
                Repeater {
                    model: 3
                    Rectangle {
                        width: 4
                        height: 4
                        y: parent.y + (8 * index)
                        color: "gold"
                        rotation: 45
                    }
                }
            }
        }

        MouseArea {
            anchors.fill: parent
            drag.target: slider
            drag.axis: Drag.XAxis
            drag.minimumX: 256
            drag.maximumX: window.width - (128 + slider.width)
        }
    }

    Rectangle {
        id: userList
        anchors.top: menuBar.bottom
        anchors.right: parent.right
        anchors.left: slider.right
        anchors.bottom: controls.top
        color: "transparent"

        Rectangle {
            border.color: "gold"
            border.width: 1
            radius: 4
            anchors.fill: parent
            anchors.rightMargin: 4
            anchors.bottomMargin: 4
            color: "transparent"
            ListView {
                anchors.fill: parent
                anchors.margins: 4
            }
        }
    }

    Rectangle {
        id: input
        anchors.left: parent.left
        anchors.right: slider.left
        anchors.bottom: parent.bottom
        height: 32
        color: "transparent"

        Rectangle {
            border.color: "gold"
            border.width: 1
            radius: 4
            anchors.fill: parent
            anchors.leftMargin: 4
            anchors.bottomMargin: 4
            color: "transparent"
            TextInput {
                id: textInput
                objectName: "textInput"
                anchors.margins: 4
                horizontalAlignment: TextInput.AlignLeft
                color: "white"
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.left: parent.left
                cursorVisible: true
                selectByMouse: true
                focus: true
                Keys.onReturnPressed: {
                    if (event.modifiers & Qt.ShiftModifier == Qt.ShiftModifier)
                        textInput.text += "\n"
                    else
                        Logic.send(textInput.text)
                    textInput.text = ""
                }
                Keys.onEnterPressed: {
                    if (event.modifiers & Qt.ShiftModifier == Qt.ShiftModifier)
                        textInput.text += "\n"
                    else
                        Logic.send(textInput.text)
                    textInput.text = ""
                }
            }
        }
    }

    Rectangle {
        id: controls
        anchors.right: parent.right
        anchors.left: slider.right
        anchors.bottom: parent.bottom
        height: 64
        width: 128
        color: "transparent"

        Rectangle {
            id: rollButton
            border.color: "gold"
            border.width: 1
            radius: 4
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottomMargin: 4
            anchors.rightMargin: 4
            color: "#c8000000"
            height: 28

            Text {
                color: "white"
                text: "Roll"
                anchors.centerIn: parent
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    window.state = "ROLL-DIALOG"
                }
            }
        }

        Rectangle {
            id: sendButton
            border.color: "gold"
            border.width: 1
            radius: 4
            anchors.top: rollButton.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.topMargin: 4
            anchors.bottomMargin: 4
            anchors.rightMargin: 4
            color: "#c8000000"
            height: 32

            Text {
                color: "white"
                text: "Send"
                anchors.centerIn: parent
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    menuBar.state = "DEFAULT"
                    Logic.send(textInput.text)
                    textInput.text = ""
                }
            }
        }
    }
}

