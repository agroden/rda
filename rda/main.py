'''
Created on Jul 15, 2012

@author: Alexander Groden
'''
import sys

from PySide.QtGui import QApplication
from rda.gui.logic import MainLogic
        
if __name__ == '__main__':
    # Create the Qt Application
    app = QApplication(sys.argv)
    # create and show the main window
    window = MainLogic()
    window.show()
    # Run the main Qt loop
    sys.exit(app.exec_())