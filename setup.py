from distutils.core import setup
import versioneer

# setup versioneer
versioneer.versionfile_source = 'rda/_version.py'
versioneer.versionfile_build = 'rda/_version.py'
versioneer.tag_prefix = ''
versioneer.parentdir_prefix = 'rda-'

setup(name = 'RDA',
    version = versioneer.get_version(),
    description = 'The Resplendent Digital Assistant client.',
    license = 'Public Domain',
    author = 'Alexander Groden',
    author_email = 'alexander.groden@gmail.com',
    url = 'https://bitbucket.org/agroden/rda',
    long_description = open('README.txt').read(),
    cmdclass = versioneer.get_cmdclass(),
    packages = ['rda', 'rda.client', 
        'rda.common', 'rda.common.commands'],
    )