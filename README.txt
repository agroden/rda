The Resplendent Digital Assistant

Website: https://bitbucket.org/agroden/rda
Author: Alexander Groden

Introduction
------------
The Resplendent Digital Assistant (RDA) is a tool to facilitate play of pen and paper games, specifically Exalted, in a networked environment.  The RDA aims to allow users to share the tabletop experience when they can't all sit down together and play.

Features
--------
Currently the RDA provides IRC-like chat and dice rolling features.

Liscence
--------
Currently the RDA is offered for use under the public domain.  This is subject to change.
