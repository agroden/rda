from optparse import OptionParser
from twisted.internet import reactor
from rda.client.network import Client, LogIn
from rda.client.chat import ChatClientProtocol
from rda.client.dice import DiceClientProtocol
from rda.common.commands import chat, dice

def connected(proto):
    print "Client connected"
    p = Client().protocol
    ChatClientProtocol(p).addChatReceivedCallback(chatReceived)
    DiceClientProtocol(p).addDiceResultCallback(diceResultReceived)
    d = LogIn(p, opts.uname, opts.passwd)
    d.addCallback(loggedIn)
    d.addErrback(failedToLogin)
    
def loggedIn(response):
    print "logged in, chatting as %s " % opts.uname
    chatLoop()
    
def chatLoop(resp=""):
    text = raw_input("Prompt: ")
    if text.startswith("/whisper "):
        doWhisper(text[len("/whisper "):])
    elif text.startswith("/roll "):
        doRoll(text[len("/roll "):])
    elif text == "/exit":
        reactor.stop()
    else:
        d = Client().protocol.callRemote(chat.PublicMessage, text = text)
        d.addCallback(chatLoop)
        d.addErrback(failedToChat)
    
def doWhisper(text):
    name = text[:text.index(' ')]
    msg = text[text.index(' ') + 1:]
    d = Client().protocol.callRemote(chat.PrivateMessage, name = name, 
                                     text = msg)
    d.addCallback(chatLoop)
    d.addErrback(failedToChat)
    
def doRoll(text):
    num = text[:text.index('d')]
    s = text[(text.index('d') + 1):]
    d = Client().protocol.callRemote(dice.DiceRequest, sides = s, number = num)
    d.addCallback(chatLoop)
    d.addErrback(failedToChat)

def chatReceived(name, text, isPrivate):
    if isPrivate:
        print "[%s] whispered: %s" % (name, text)
    else:
        print "[%s] %s" % (name, text)
    
def diceResultReceived(name, values):
    print "[%s] rolled: {%s}" % (name, ', '.join(str(val) for val in values))

def failedToConnect(error):
    print "failed to connect"
    print error
    
def failedToLogin(error):
    print "failed to login"
    print error
    
def failedToChat(error):
    print "failed to send chat message"
    print error

usage = "usage: %prog [options]"
parser = OptionParser()
parser.add_option("-n", "--host", default="127.0.0.1", dest="host",
                  help="The host to connect to [default: %default]")
parser.add_option("-p", "--port", default=9000, dest="port",
                  help="The port to connect to [default: %default]")
parser.add_option("-u", "--uname", default="guest", dest="uname",
                  help="The name to use to log on with [default: %default]")
parser.add_option("-w", "--passwd", default="passwd", dest="passwd",
                  help="The password to use to log into the server")

(opts, args) = parser.parse_args()

print "Starting RDA client, connecting to %s:%d" % (opts.host, int(opts.port))
client = Client()
connectAttempt = client.connectTo(opts.host, opts.port)
connectAttempt.addCallback(connected)
connectAttempt.addErrback(failedToConnect)
reactor.run()